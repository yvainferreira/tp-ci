let factorielle = (value) => {
	if (value < 0) {
		throw 'Cannot be negative'
	}
	else if (value > 1) {
		return value * factorielle(value-1)
	}
	else {
		return 1
	}
}

exports.factorielle = factorielle